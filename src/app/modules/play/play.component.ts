import { Component, OnInit } from '@angular/core';
import { QuestionsService } from '../../core/services/questions.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { QuestionModel } from '../../core/state/question.model';

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.scss'],
})
export class PlayComponent implements OnInit {
  questions$ = this.questionsService.questions$;
  gettingQuestions$ = this.questionsService.gettingQuestions$;
  getQuestionsSubscription: Subscription = this.route.queryParams
    .pipe(
      switchMap((params) =>
        this.questionsService.getQuestions({
          type: params.type,
          amount: params.amount,
          difficulty: params.difficulty,
        })
      )
    )
    .subscribe(
      () => {
        this.apiError = '';
      },
      (err) => {
        this.apiError = err;
      }
    );
  isFinished = false;
  rightAnswers = 0;
  wrongAnswers = 0;
  apiError = '';

  constructor(
    private readonly route: ActivatedRoute,
    private readonly questionsService: QuestionsService
  ) {}

  ngOnInit(): void {
    this.questions$.subscribe((res) => {
      const answeredQuestions = res.filter((val) => val.selectedId).length;
      this.apiError = '';
      this.rightAnswers = res.reduce((acc, val) => {
        const rightAnswerId = val.answers.find(
          (answer) => answer.isCorrect
        )!._id;
        return val.selectedId === rightAnswerId ? acc + 1 : acc;
      }, 0);
      this.wrongAnswers = res.length - this.rightAnswers;
      this.isFinished = !!res.length && answeredQuestions === res.length;
    });
  }

  onAnswerClicked(
    questionId: QuestionModel['_id'],
    answerSelected: string
  ): void {
    this.questionsService.selectAnswer(questionId, answerSelected);
  }
}
